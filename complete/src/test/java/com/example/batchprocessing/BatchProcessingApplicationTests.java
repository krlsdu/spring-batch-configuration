package com.example.batchprocessing;

import com.example.batchprocessing.BatchConfiguration;
import com.example.batchprocessing.JobCompletionNotificationListener;
import com.example.batchprocessing.PersonItemProcessor;
import com.example.batchprocessing.Person;

import static org.junit.Assert.assertEquals;

import javax.sql.DataSource;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.context.ApplicationContext;
import static org.junit.Assert.*;

@SpringBootTest
@SpringBatchTest
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Sql("/schema-all.sql")
@TestPropertySource(properties = "spring.batch.job.enabled=false")
class BatchProcessingApplicationTests {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    // @Test
    // void contextLoads() throws Exception {
    // JobExecution jobExecution = jobLauncherTestUtils.launchJob();
    // assertEquals(BatchStatus.COMPLETED,
    // jobExecution.getExitStatus().getExitCode());
    // }

    @Test
    void stepLoads() throws Exception {
        JobExecution jobExecution = jobLauncherTestUtils.launchStep("step1");
        assertEquals(BatchStatus.COMPLETED, jobExecution.getExitStatus().getExitCode());
    }

}